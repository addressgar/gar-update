FROM adoptopenjdk/openjdk11

COPY ./target/garupdate*.jar /opt/garupdate/garupdate.jar

ENV JAVA_OPTS="-XX:+AlwaysActAsServerClassMachine -XX:+UseContainerSupport -XX:+ExitOnOutOfMemoryError -XX:+PerfDisableSharedMem"
ENV TZ=Europe/Moscow

CMD ["java", "-jar", "/opt/garupdate/garupdate.jar"]
