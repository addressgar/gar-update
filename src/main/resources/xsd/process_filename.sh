#!/bin/bash

# Преобразовывает имена файлов 
# из "AS_ADDR_OBJ_2_251_01_04_01_01.xsd" в "addr_obj.xsd"
for filename in $(ls *.xsd)
do
if [ -f "$filename" ]
	then
		new_filename=${filename%%[[:digit:]]*}
		new_filename="${new_filename%?}.xsd"
		new_filename=${new_filename:3}
		new_filename=${new_filename,,}
		mv $filename $new_filename
		echo "$filename moved to $new_filename"
	fi
done