create table if not exists gar.house_type (
	id bigint not null primary key,
	name text not null,
	shortname text,
	description text
);

create table if not exists gar.address_object_type (
	id bigint not null primary key,
	level bigint not null,
	name text not null,
	shortname text not null,
	description text
);

create table gar.apartment_type(
	id bigint not null primary key,
	shortname text,
	name text,
	description text
);

create table if not exists gar.add_house_type (
	id bigint not null primary key,
	name text not null,
	shortname text,
	description text
);

create table if not exists gar.normative_document_kind (
	id bigint not null primary key,
	name text not null
);

create table if not exists gar.normative_document_type (
	id bigint not null primary key,
	name text not null
);

create table if not exists gar.object_level (
	level bigint not null primary key,
	name text not null,
	shortname text
);

create table if not exists gar.operation_type (
	id bigint not null primary key,
	name text not null,
	shortname text,
	description text
);

create table if not exists gar.param_type (
	id bigint not null primary key,
	name text not null,
	shortname text,
	description text
);

create table if not exists gar.room_type (
	id bigint not null primary key,
	name text not null,
	shortname text,
	description text
);

create table if not exists gar.address_object (
	object_id bigint not null primary key,
	object_guid text not null,
	name text not null,
	type_name text not null,
	level bigint not null,
	operation_type_id bigint
);

create table if not exists gar.administrative_hierarchy (
	object_id bigint not null primary key,
	parent_object_id bigint,
	region_code text,
	area_code text,
	city_code text,
	place_code text,
	plan_code text,
	street_code text,
	path text
);

create table if not exists gar.apartment (
	object_id bigint not null primary key,
	object_guid text not null,
	number text not null,
	apartment_type bigint not null
);

create table if not exists gar.carplace (
	object_id bigint not null primary key,
	object_guid text not null,
	number text not null
);

create table if not exists gar.house (
	object_id bigint not null primary key,
	object_guid text not null,
	housenum text,
	addnum1 text,
	addnum2 text,
	house_type bigint,
	addtype1 bigint,
	addtype2 bigint
);

create table if not exists gar.municipal_hierarchy (
	object_id bigint not null primary key,
	parent_object_id bigint,
	oktmo text,
	path text
);

create table if not exists gar.normative_document (
	object_id bigint not null primary key,
	name text not null,
	document_date date not null,
	number text not null,
	type bigint not null,
	kind bigint not null,
	orgname text,
	regnum text,
	regdate date,
	accdate date,
	comment text
);

create table if not exists gar.room (
	object_id bigint not null primary key,
	object_guid text not null,
	number text not null,
	room_type bigint not null,
	operation_type_id bigint not null
);

create table if not exists gar.stead (
	object_id bigint not null primary key,
	object_guid text not null,
	number text not null,
	operation_type_id bigint not null
);

