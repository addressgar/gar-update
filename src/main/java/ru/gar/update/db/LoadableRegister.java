package ru.gar.update.db;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.db.loader.Loadable;

public interface LoadableRegister {
    void registerLoaderBean(GarDirectoryType type, Loadable loader);
    Loadable getLoaderBean(GarDirectoryType type);
}
