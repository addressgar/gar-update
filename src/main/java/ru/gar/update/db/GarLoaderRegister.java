package ru.gar.update.db;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.db.loader.Loadable;

import java.util.HashMap;
import java.util.Map;

@Service
@Scope("singleton")
public class GarLoaderRegister implements LoadableRegister {
    private final Map<GarDirectoryType, Loadable> loadersMap = new HashMap<>();

    @Override
    public void registerLoaderBean(GarDirectoryType type, Loadable loader) {
        loadersMap.put(type, loader);
    }

    @Override
    public Loadable getLoaderBean(GarDirectoryType type) {
        return loadersMap.get(type);
    }
}
