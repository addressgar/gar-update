package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.admhierarchy.ITEMS;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.ADMINISTRATIVE_HIERARCHY)
public final class AdministrativeHierarchyLoader extends AbstractLoader  {
    public AdministrativeHierarchyLoader() {
        super();
    }

    private ITEMS getAdministrativeHierarchy(){
        Object xjb = getGarElement().getXjbObject();
        return (ITEMS) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getAdministrativeHierarchy().getITEM().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.administrative_hierarchy(object_id, parent_object_id, region_code, area_code, city_code, place_code, plan_code, street_code, path)\n" +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (parent_object_id, region_code, area_code, city_code, place_code, plan_code, street_code, path)\n" +
                "    = (excluded.parent_object_id, excluded.region_code, excluded.area_code, excluded.city_code, excluded.place_code, excluded.plan_code, excluded.street_code, excluded.path);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        ITEMS.ITEM admHierarchy = (ITEMS.ITEM) row;
        statement.setLong(1, admHierarchy.getOBJECTID());
        statement.setLong(2, admHierarchy.getPARENTOBJID());
        statement.setString(3, admHierarchy.getREGIONCODE());
        statement.setString(4, admHierarchy.getAREACODE());
        statement.setString(5, admHierarchy.getCITYCODE());
        statement.setString(6, admHierarchy.getPLACECODE());
        statement.setString(7, admHierarchy.getPLANCODE());
        statement.setString(8, admHierarchy.getSTREETCODE());
        statement.setString(9, admHierarchy.getPATH());
    }
}
