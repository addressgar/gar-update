package ru.gar.update.db.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.gar.update.db.GarElement;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

public abstract class AbstractLoader implements Loadable {
    private DataSource dataSource;
    private GarElement garElement;

    @Value("${db.pack.size:10000}")
    private Integer packSize;

    @Override
    public void load(GarElement garElement) throws SQLException {
        setGarElement(garElement);
        initLoad();

        try(Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(getInsertSql())) {
            int batchesCount = 0;
            boolean supportsBatchUpdates = connection.getMetaData().supportsBatchUpdates();

            Iterator<?> it = getIterator();
            while(it.hasNext()) {
                Object row = it.next();
                if(row != null) {
                    addBatchRow(statement, row);
                }

                if (supportsBatchUpdates) {
                    statement.addBatch();

                    if(needExecuteBatch(++batchesCount)) {
                        statement.executeBatch();
                    }
                } else {
                    statement.executeUpdate();
                }
            }
            if(supportsBatchUpdates && existsNonExecutedBatches(batchesCount)) {
                statement.executeBatch();
            }
        }
    }

    private void initLoad() {
        checkCorrectPackSize();
    }

    private void checkCorrectPackSize() {
        if (packSize == null) {
            throw new IllegalArgumentException("Объем пачки не может быть пустым");
        }
        if (packSize < 1) {
            throw new IllegalArgumentException(String.format("Объем пачки не может быть меньше единицы. Текущее значение: %d", packSize));
        }
    }

    private boolean existsNonExecutedBatches(int batchesCount) {
        return batchesCount % packSize != 0;
    }

    private boolean needExecuteBatch(int batchesCount) {
        return batchesCount % packSize == 0;
    }

    public AbstractLoader() {
        super();
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private void setGarElement(GarElement garElement) {
        this.garElement = garElement;
    }

    protected int getPackSize() {
        return packSize;
    }

    protected DataSource getDataSource() {
        return dataSource;
    }

    private Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    protected GarElement getGarElement() {
        return garElement;
    }

    protected abstract Iterator<?> getIterator();
    protected abstract String getInsertSql();
    protected abstract void addBatchRow(PreparedStatement statement, Object row) throws SQLException;
}
