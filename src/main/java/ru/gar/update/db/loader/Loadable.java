package ru.gar.update.db.loader;

import ru.gar.update.db.GarElement;

import java.sql.SQLException;

public interface Loadable {
    default void load(GarElement garElement) throws SQLException {
        throw new UnsupportedOperationException(String.format("Загрузка данных для класса %s не реализована", this.getClass()));
    };
}
