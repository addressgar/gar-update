package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.ADDRESSOBJECTS;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.ADDRESS_OBJECTS)
public final class AddressObjectLoader extends AbstractLoader {
    public AddressObjectLoader() {
        super();
    }

    private ADDRESSOBJECTS getAddressObjects(){
        Object xjb = getGarElement().getXjbObject();
        return (ADDRESSOBJECTS) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getAddressObjects().getOBJECT().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.address_object(object_id, object_guid, name, type_name, level, operation_type_id)\n" +
                "values (?, ?, ?, ?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (object_guid, name, type_name, level, operation_type_id) = (excluded.object_guid, excluded.name, excluded.type_name, excluded.level, excluded.operation_type_id);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        ADDRESSOBJECTS.OBJECT address = (ADDRESSOBJECTS.OBJECT) row;
        statement.setLong(1, address.getOBJECTID());
        statement.setString(2, address.getOBJECTGUID());
        statement.setString(3, address.getNAME());
        statement.setString(4, address.getTYPENAME());
        statement.setLong(5, Long.parseLong(address.getLEVEL()));
        statement.setLong(6, address.getOPERTYPEID().longValue());
    }
}
