package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.ROOMTYPES;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.ROOM_TYPES)
public final class RoomTypeLoader extends AbstractLoader {
    public RoomTypeLoader() {
        super();
    }

    private ROOMTYPES getRoomTypes(){
        Object xjb = getGarElement().getXjbObject();
        return (ROOMTYPES) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getRoomTypes().getROOMTYPE().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.room_type(id, name, shortname, description)\n" +
                "values (?, ?, ?, ?)\n" +
                "on conflict(id) do update\n" +
                "set (name, shortname, description) = (excluded.name, excluded.shortname, excluded.description);";
    }

    @Override
    @SuppressWarnings("Duplicates")
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        ROOMTYPES.ROOMTYPE roomType = (ROOMTYPES.ROOMTYPE) row;
        statement.setLong(1, roomType.getID().longValue());
        statement.setString(2, roomType.getNAME());
        statement.setString(3, roomType.getSHORTNAME());
        statement.setString(4, roomType.getDESC());
    }
}
