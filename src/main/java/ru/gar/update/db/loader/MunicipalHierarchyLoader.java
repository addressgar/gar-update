package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.xjb.munhierarchy.ITEMS;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.MUNICIPAL_HIERARCHY)
public final class MunicipalHierarchyLoader extends AbstractLoader {
    public MunicipalHierarchyLoader() {
        super();
    }

    private ITEMS getMunicipalHierarchy(){
        Object xjb = getGarElement().getXjbObject();
        return (ITEMS) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getMunicipalHierarchy().getITEM().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.municipal_hierarchy(object_id, parent_object_id, oktmo, path)\n" +
                "values (?, ?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (parent_object_id, oktmo, path) = (excluded.parent_object_id, excluded.oktmo, excluded.path);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        ITEMS.ITEM munHierarchy = (ITEMS.ITEM) row;
        statement.setLong(1, munHierarchy.getOBJECTID());
        statement.setLong(2, munHierarchy.getPARENTOBJID());
        statement.setString(3, munHierarchy.getOKTMO());
        statement.setString(4, munHierarchy.getPATH());
    }
}
