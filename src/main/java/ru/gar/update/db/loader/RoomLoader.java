package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.ROOMS;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.ROOMS)
public final class RoomLoader extends AbstractLoader  {
    public RoomLoader() {
        super();
    }

    private ROOMS getRooms(){
        Object xjb = getGarElement().getXjbObject();
        return (ROOMS) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getRooms().getROOM().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.room(object_id, object_guid, \"number\", room_type, operation_type_id)\n" +
                "values (?, ?, ?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (object_guid, \"number\", room_type, operation_type_id)\n" +
                "    = (excluded.object_guid, excluded.\"number\", excluded.room_type, excluded.operation_type_id);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        ROOMS.ROOM room = (ROOMS.ROOM) row;
        statement.setLong(1, room.getOBJECTID());
        statement.setString(2, room.getOBJECTGUID());
        statement.setString(3, room.getNUMBER());
        statement.setLong(4, room.getROOMTYPE().longValue());
        statement.setLong(5, room.getOPERTYPEID().longValue());
    }
}
