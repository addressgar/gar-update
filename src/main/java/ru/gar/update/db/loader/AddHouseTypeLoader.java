package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.xjb.common.HOUSETYPES;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.ADD_HOUSE_TYPES)
public final class AddHouseTypeLoader extends AbstractLoader {

    public AddHouseTypeLoader() {
        super();
    }

    private HOUSETYPES getAddHouseTypes(){
        Object xjb = getGarElement().getXjbObject();
        return (HOUSETYPES) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getAddHouseTypes().getHOUSETYPE().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.add_house_type(id, name, shortname, description)\n" +
                "values (?, ?, ?, ?)\n" +
                "on conflict(id) do update\n" +
                "set (name, shortname, description) = (excluded.name, excluded.shortname, excluded.description)";
    }

    @Override
    @SuppressWarnings("Duplicates")
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        HOUSETYPES.HOUSETYPE houseType = (HOUSETYPES.HOUSETYPE) row;
        statement.setLong(1, houseType.getID().longValue());
        statement.setString(2, houseType.getNAME());
        statement.setString(3, houseType.getSHORTNAME());
        statement.setString(4, houseType.getDESC());
    }
}
