package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.CARPLACES;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.CARPLACES)
public final class CarplaceLoader extends AbstractLoader  {
    public CarplaceLoader() {
        super();
    }

    private CARPLACES getCarplaces(){
        Object xjb = getGarElement().getXjbObject();
        return (CARPLACES) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getCarplaces().getCARPLACE().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.carplace(object_id, object_guid, \"number\")\n" +
                "values (?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (object_guid, \"number\") = (excluded.object_guid, excluded.\"number\");";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        CARPLACES.CARPLACE carplace = (CARPLACES.CARPLACE) row;
        statement.setLong(1, carplace.getOBJECTID());
        statement.setString(2, carplace.getOBJECTGUID());
        statement.setString(3, carplace.getNUMBER());
    }
}
