package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.HOUSES;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.HOUSES)
public final class HouseLoader extends AbstractLoader  {
    public HouseLoader() {
        super();
    }

    private HOUSES getHouses(){
        Object xjb = getGarElement().getXjbObject();
        return (HOUSES) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getHouses().getHOUSE().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.house(object_id, object_guid, housenum, addnum1, addnum2, house_type, addtype1, addtype2)\n" +
                "values (?, ?, ?, ?, ?, ?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (object_guid, housenum, addnum1, addnum2, house_type, addtype1, addtype2)\n" +
                "    = (excluded.object_guid, excluded.housenum, excluded.addnum1, excluded.addnum2, excluded.house_type, excluded.addtype1, excluded.addtype2);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        HOUSES.HOUSE house = (HOUSES.HOUSE) row;
        statement.setLong(1, house.getOBJECTID());
        statement.setString(2, house.getOBJECTGUID());
        statement.setString(3, house.getHOUSENUM());
        statement.setString(4, house.getADDNUM1());
        statement.setString(5, house.getADDNUM2());
        statement.setLong(6, house.getHOUSETYPE().longValue());
        statement.setLong(7, house.getADDTYPE1().longValue());
        statement.setLong(8, house.getADDTYPE2().longValue());
    }
}
