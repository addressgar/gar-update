package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.HOUSETYPES;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.HOUSE_TYPES)
public final class HouseTypeLoader extends AbstractLoader {
    public HouseTypeLoader() {
        super();
    }

    private HOUSETYPES getHouseTypes(){
        Object xjb = getGarElement().getXjbObject();
        return (HOUSETYPES) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getHouseTypes().getHOUSETYPE().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.house_type(id, name, shortname, description)\n" +
                "values (?, ?, ?, ?)\n" +
                "on conflict(id) do update\n" +
                "set (name, shortname, description)\n" +
                " = (excluded.name, excluded.shortname, excluded.description);";
    }

    @Override
    @SuppressWarnings("Duplicates")
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        HOUSETYPES.HOUSETYPE houseType = (HOUSETYPES.HOUSETYPE) row;
        statement.setLong(1, houseType.getID().longValue());
        statement.setString(2, houseType.getNAME());
        statement.setString(3, houseType.getSHORTNAME());
        statement.setString(4, houseType.getDESC());
    }
}
