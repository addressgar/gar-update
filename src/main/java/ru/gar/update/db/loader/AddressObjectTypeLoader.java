package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.ADDRESSOBJECTTYPES;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.ADDRESS_OBJECT_TYPES)
public final class AddressObjectTypeLoader extends AbstractLoader {
    public AddressObjectTypeLoader() {
        super();
    }

    private ADDRESSOBJECTTYPES getAddressObjectTypes(){
        Object xjb = getGarElement().getXjbObject();
        return (ADDRESSOBJECTTYPES) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getAddressObjectTypes().getADDRESSOBJECTTYPE().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.address_object_type(id, level, name, shortname, description)\n" +
                "values (?, ?, ?, ?, ?)\n" +
                "on conflict(id) do update\n" +
                "set (level, name, shortname, description) = (excluded.level, excluded.name, excluded.shortname, excluded.description)";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        ADDRESSOBJECTTYPES.ADDRESSOBJECTTYPE addressType = (ADDRESSOBJECTTYPES.ADDRESSOBJECTTYPE) row;
        statement.setLong(1, addressType.getID().longValue());
        statement.setLong(2, addressType.getLEVEL().longValue());
        statement.setString(3, addressType.getNAME());
        statement.setString(4, addressType.getSHORTNAME());
        statement.setString(5, addressType.getDESC());
    }
}
