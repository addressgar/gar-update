package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.APARTMENTTYPES;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.APARTMENT_TYPES)
public final class ApartmentTypeLoader extends AbstractLoader  {
    public ApartmentTypeLoader() {
        super();
    }

    private APARTMENTTYPES getApartmentTypes(){
        Object xjb = getGarElement().getXjbObject();
        return (APARTMENTTYPES) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getApartmentTypes().getAPARTMENTTYPE().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.apartment_type(id, shortname, name, description)\n" +
                "values (?, ?, ?, ?)\n" +
                "on conflict(id) do update\n" +
                "set (shortname, name, description) = (excluded.shortname, excluded.name, excluded.description);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        APARTMENTTYPES.APARTMENTTYPE apartmentType = (APARTMENTTYPES.APARTMENTTYPE) row;
        statement.setLong(1, apartmentType.getID().longValue());
        statement.setString(2, apartmentType.getSHORTNAME());
        statement.setString(3, apartmentType.getNAME());
        statement.setString(4, apartmentType.getDESC());
    }
}
