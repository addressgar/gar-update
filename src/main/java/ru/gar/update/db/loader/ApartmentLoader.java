package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.APARTMENTS;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.APARTMENTS)
public final class ApartmentLoader extends AbstractLoader  {
    public ApartmentLoader() {
        super();
    }

    private APARTMENTS getApartments(){
        Object xjb = getGarElement().getXjbObject();
        return (APARTMENTS) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getApartments().getAPARTMENT().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.apartment(object_id, object_guid, \"number\", apartment_type)\n" +
                "values (?, ?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (object_guid, \"number\", apartment_type) = (excluded.object_guid, excluded.\"number\", excluded.apartment_type);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        APARTMENTS.APARTMENT apartment = (APARTMENTS.APARTMENT) row;
        statement.setLong(1, apartment.getOBJECTID());
        statement.setString(2, apartment.getOBJECTGUID());
        statement.setString(3, apartment.getNUMBER());
        statement.setLong(4, apartment.getAPARTTYPE().longValue());
    }
}
