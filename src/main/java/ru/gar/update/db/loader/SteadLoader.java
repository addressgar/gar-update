package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.STEADS;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.STEADS)
public final class SteadLoader extends AbstractLoader  {
    public SteadLoader() {
        super();
    }

    private STEADS getSteads(){
        Object xjb = getGarElement().getXjbObject();
        return (STEADS) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getSteads().getSTEAD().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.stead(object_id, object_guid, \"number\", operation_type_id)\n" +
                "values (?, ?, ?, ?)\n" +
                "on conflict(object_id) do update\n" +
                "set (object_guid, \"number\", operation_type_id) = (excluded.object_guid, excluded.\"number\", excluded.operation_type_id);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        STEADS.STEAD stead = (STEADS.STEAD) row;
        statement.setLong(1, stead.getOBJECTID().longValue());
        statement.setString(2, stead.getOBJECTGUID());
        statement.setString(3, stead.getNUMBER());
        statement.setLong(4, Long.parseLong(stead.getOPERTYPEID()));
    }
}
