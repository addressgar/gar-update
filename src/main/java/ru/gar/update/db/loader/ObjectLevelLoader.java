package ru.gar.update.db.loader;

import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.exception.NotYetImplementedException;
import ru.gar.update.xjb.common.OBJECTLEVELS;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

@LoaderFor(GarDirectoryType.OBJECT_LEVELS)
public final class ObjectLevelLoader extends AbstractLoader  {
    public ObjectLevelLoader() {
        super();
    }

    private OBJECTLEVELS getObjectLevels(){
        Object xjb = getGarElement().getXjbObject();
        return (OBJECTLEVELS) xjb;
    }

    @Override
    protected Iterator<?> getIterator() {
        return getObjectLevels().getOBJECTLEVEL().iterator();
    }

    @Override
    protected String getInsertSql() {
        return "insert into gar.object_level(level, name, shortname)\n" +
                "values (?, ?, ?)\n" +
                "on conflict(level) do update\n" +
                "set (name, shortname) = (excluded.name, excluded.shortname);";
    }

    @Override
    protected void addBatchRow(PreparedStatement statement, Object row) throws SQLException {
        OBJECTLEVELS.OBJECTLEVEL objectLevel = (OBJECTLEVELS.OBJECTLEVEL) row;
        statement.setLong(1, objectLevel.getLEVEL().longValue());
        statement.setString(2, objectLevel.getNAME());
        statement.setString(3, objectLevel.getSHORTNAME());
    }
}
