package ru.gar.update.db;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import ru.gar.update.db.loader.Loadable;

import java.sql.SQLException;

@Service
@Scope("singleton")
public final class GarLoader {
    private final LoadableRegister loadableRegister;

    public void load(GarElement garElement) throws NoSuchMethodException, SQLException {
        Loadable loader = getCorrectLoader(garElement);
        loader.load(garElement);
    }

    private Loadable getCorrectLoader(GarElement garElement) throws NoSuchMethodException {
        Loadable loader = loadableRegister.getLoaderBean(garElement.getGarDirectoryType());
        if(loader == null) {
            throw new NoSuchMethodException(String.format("Отсуствует реализация загрузчика для типа данных %s (%s)",  garElement.getGarDirectoryType(), garElement.getXjbObject().getClass().toString()));
        }
        return loader;
    }

    @Autowired
    public GarLoader(LoadableRegister loadableRegister) {
        super();
        this.loadableRegister = loadableRegister;
    }
}
