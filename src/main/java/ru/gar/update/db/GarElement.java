package ru.gar.update.db;

import ru.gar.update.config.GarDirectoryType;

public final class GarElement {
    private final Object xjbObject;

    public Object getXjbObject() {
        return xjbObject;
    }

    public GarDirectoryType getGarDirectoryType() {
        return garDirectoryType;
    }

    private final GarDirectoryType garDirectoryType;

    public GarElement(Object xjbObject, GarDirectoryType garDirectoryType) {
        if (!garDirectoryType.getXjbClass().isInstance(xjbObject)) {
            throw new IllegalArgumentException(String.format("Неправильный тип объекта %s. Получен %s, должен быть %s",
                    garDirectoryType.toString(), xjbObject.getClass().toString(), garDirectoryType.getXjbClass().toString()));
        }
        this.xjbObject = xjbObject;
        this.garDirectoryType = garDirectoryType;
    }
}
