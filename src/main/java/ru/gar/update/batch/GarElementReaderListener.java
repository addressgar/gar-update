package ru.gar.update.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.gar.update.event.Event;
import ru.gar.update.event.EventRegister;
import ru.gar.update.event.EventType;

@Service
@Qualifier("garElementReaderListener")
@StepScope
public class GarElementReaderListener implements ItemReadListener<GarElementReader> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final EventRegister eventRegister;

    @Value("#{jobParameters['xml.file']}")
    private String filename;

    @Autowired
    public GarElementReaderListener(@Qualifier("garLoadingEventRegister") EventRegister eventRegister) {
        this.eventRegister = eventRegister;
    }

    @Override
    public void beforeRead() {

    }

    @Override
    public void afterRead(GarElementReader garElementReader) {
        String message = String.format("Файл %s успешно прочитан", filename);
        logger.info(message);
    }

    @Override
    public void onReadError(Exception e) {
        String message = String.format("Во время выполнения загрузки файла %s возникла ошибка: %s", filename, e.getMessage());
        logger.error(message);
        eventRegister.addEvent(new Event(EventType.ERROR, message));
    }
}
