package ru.gar.update.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
@StepScope
public class DeleteDataFilesTasklet implements Tasklet {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("#{jobParameters['xml.file']}")
    private String filename;

    @Value("${need.delete.data.files}")
    private boolean isNeedDeleteDataFiles;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        if (filename != null && isNeedDeleteDataFiles) {
            File file = new File(filename);
            if (file.exists()) {
                if (file.delete()) {
                    logger.info(String.format("Удалён файл %s", filename));
                } else {
                    logger.warn(String.format("Не удалось удалить файл %s", filename));
                }
            }
        }
        return RepeatStatus.FINISHED;
    }
}
