package ru.gar.update.batch;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.gar.update.config.GarDirectoryType;
import ru.gar.update.db.GarElement;

import java.io.File;

@Service
@StepScope
public class GarElementReader implements ItemReader<GarElement> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final File xmlFile;
    private final GarDirectoryType garDirectoryType;

    private boolean isFileRead = false;

    public GarElementReader(@Value("#{jobParameters['xml.file']}") final String xmlFileName) {
        this.xmlFile = new File(xmlFileName);
        this.garDirectoryType = GarDirectoryType.getGarDirectoryType(xmlFile.getName());
    }

    @Override
    public GarElement read() throws JAXBException {
        if(isFileRead())
            return null;

        GarDirectoryType directoryType = getDirectoryType();
        if (directoryType == null) {
            logger.warn(String.format("Файл %s пропущен, неизвестный тип файла", xmlFile));
        }
        GarElement garElement = getGarElement(directoryType);
        setFileRead();
        return garElement;
    }

    private GarElement getGarElement(GarDirectoryType directoryType) throws JAXBException {
        if (directoryType == null)
            return null;

        JAXBContext jaxbContext = JAXBContext.newInstance(directoryType.getXjbClass());
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Object data = unmarshaller.unmarshal(xmlFile);
        return new GarElement(data, directoryType);
    }

    private void setFileRead() {
        isFileRead = true;
    }

    private GarDirectoryType getDirectoryType() {
        return garDirectoryType;
    }

    private boolean isFileRead() {
        return isFileRead;
    }

}
