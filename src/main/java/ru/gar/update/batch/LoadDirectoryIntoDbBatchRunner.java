package ru.gar.update.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import ru.gar.update.event.Event;
import ru.gar.update.event.EventRegister;
import ru.gar.update.event.EventType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class LoadDirectoryIntoDbBatchRunner {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Job loadGarDirectoryJob;
    private final JobLauncher jobLauncher;
    private final ThreadPoolTaskExecutor taskExecutor;
    private final EventRegister eventRegister;

    @Value("${data.path}")
    private String dataPath;

    public void run() {
        try {
            Files.walk(Paths.get(dataPath))
                    .filter(LoadDirectoryIntoDbBatchRunner::isXmlFile)
                    .forEach( file -> {
                        JobParameters jobParameters = new JobParametersBuilder()
                                .addString("xml.file", file.toAbsolutePath().toString())
                                .addLong("run.currentTimeMillis", System.currentTimeMillis())
                                .toJobParameters();
                        try {
                            jobLauncher.run(loadGarDirectoryJob, jobParameters);
                        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                            String message = String.format("При постановке задачи возникла ошибка: %s", e.getMessage());
                            eventRegister.addEvent(new Event(EventType.ERROR, message));
                            logger.error(message);
                        }

                    });
            taskExecutor.shutdown();
            eventRegister.sendNotifications();
            eventRegister.clearEvents();

        } catch (IOException e) {
            logger.warn(String.format("Во время обработки файлов в директории dataPath возникла ошибка: %s", e));
        }
    }

    private static boolean isXmlFile(Path path) {
        return path != null
                && Files.isRegularFile(path)
                && path.toString().toLowerCase().endsWith(".xml");
    }

    @Autowired
    public LoadDirectoryIntoDbBatchRunner(@Qualifier("LoadDirectoryIntoDbBatch") Job loadDirectoryBatch,
                                          JobLauncher jobLauncher,
                                          @Qualifier("garThreadExecutor") ThreadPoolTaskExecutor taskExecutor,
                                          @Qualifier("garLoadingEventRegister") EventRegister eventRegister) {
        this.loadGarDirectoryJob = loadDirectoryBatch;
        this.jobLauncher = jobLauncher;
        this.taskExecutor = taskExecutor;
        this.eventRegister = eventRegister;
    }
}
