package ru.gar.update.batch;

import org.jetbrains.annotations.NotNull;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.gar.update.db.GarElement;

@Configuration
@EnableBatchProcessing
public class LoadDirectoryIntoDbBatch {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final ItemWriter<? super GarElement> garElementWriter;
    private final ItemReader<? extends GarElement> garElementReader;
    private final Tasklet deleteDataFilesTasklet;
    private final ItemReadListener<GarElementReader> garElementReaderListener;

    @Bean
    @Qualifier("LoadDirectoryIntoDbBatch")
    public Job loadGarDirectoryJob() {

        return jobBuilderFactory.get("LOAD-GAR-DIRECTORY")
                .incrementer(getJobParametersIncrementer())
                .start(loadDataFileIntoDb())
                .next(deleteDataFile())
                .build();
    }

    @Bean
    public Step loadDataFileIntoDb() {
        return stepBuilderFactory.get("loadDataFileIntoDb")
                .<GarElement, GarElement> chunk(1)
                .reader(garElementReader)
                .listener(garElementReaderListener)
                .writer(garElementWriter)
                .build();
    }

    @Bean
    public Step deleteDataFile() {
        return stepBuilderFactory.get("deleteDataFile")
                .tasklet(deleteDataFilesTasklet)
                .build();
    }

    @NotNull
    @Bean
    JobParametersIncrementer getJobParametersIncrementer() {
        return new RunIdIncrementer();
    }

    @Autowired
    public LoadDirectoryIntoDbBatch(JobBuilderFactory jobBuilderFactory,
                                    StepBuilderFactory stepBuilderFactory,
                                    ItemWriter<? super GarElement> garElementWriter,
                                    ItemReader<? extends GarElement> garElementReader,
                                    Tasklet deleteDataFilesTasklet,
                                    @Qualifier("garElementReaderListener") ItemReadListener<GarElementReader> garElementReaderListener) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.garElementWriter = garElementWriter;
        this.garElementReader = garElementReader;
        this.deleteDataFilesTasklet = deleteDataFilesTasklet;
        this.garElementReaderListener = garElementReaderListener;
    }
}
