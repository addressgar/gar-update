package ru.gar.update.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gar.update.db.GarElement;
import ru.gar.update.db.GarLoader;

import java.util.List;

@Service
@StepScope
public class GarElementWriter implements ItemWriter<GarElement> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final GarLoader garLoader;

    @Autowired
    public GarElementWriter(GarLoader garLoader) {
        super();
        this.garLoader = garLoader;
    }

    @Override
    public void write(List<? extends GarElement> list) throws Exception {
        for (GarElement garElement: list) {
            if(garElement == null) {
                logger.warn("Пустой элемент в списке GarElement");
                continue;
            }

            try {
                garLoader.load(garElement);
            } catch (Exception ex) {
                logger.warn(String.format("Во время записи элемента %s в БД возникла ошибка: %s", garElement.getGarDirectoryType(), ex));
                throw ex;
            }
        }
    }
}
