package ru.gar.update.exception;

public class NotYetImplementedException extends RuntimeException {
    @Deprecated
    public NotYetImplementedException() {
        super();
    }

    @Deprecated
    public NotYetImplementedException(String message) {
        super(message);
    }
}
