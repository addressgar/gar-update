package ru.gar.update.broker;

public interface BrokerProducer {
    void sendToQueue(String queue, Message message);
}
