package ru.gar.update.broker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.gar.update.event.EventMessage;

public class LoadingStateMessage implements Message {
    private final EventMessage message;

    @Override
    public String getMessage() {
        if (message == null) {
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.toJson(message);
    }

    public LoadingStateMessage(EventMessage message) {
        super();
        this.message = message;
    }
}
