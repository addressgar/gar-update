package ru.gar.update.broker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
@Qualifier("activeMqBrokerProducer")
@Scope("singleton")
public class ActiveMqProducer implements BrokerProducer {
    private final JmsTemplate jmsTemplate;

    @Override
    public void sendToQueue(String queue, Message message) {
        if(queue == null
                || message == null
                || message.getMessage() == null
                || "".equals(message.getMessage())) {
            return;
        }

        jmsTemplate.send(queue, session -> session.createTextMessage(message.getMessage()));
    }

    @Autowired
    public ActiveMqProducer(@Qualifier("activeMqJmsTemplate")JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }
}
