package ru.gar.update;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.gar.update.config.GarUpdateSchedulingConfig;

@SpringBootApplication
@EnableScheduling
public class GarUpdateApplication {
    public static void main(String[] args) {
        SpringApplication.run(GarUpdateApplication.class, args);
    }
}
