package ru.gar.update.event;

import java.util.List;

public interface EventRegister {
    void addEvent(Event event);
    List<Event> getEvents();
    void clearEvents();
    boolean hasErrorEvents();
    void sendNotifications();
}
