package ru.gar.update.event;

import ru.gar.update.exception.NotYetImplementedException;

public class GarLoadingNotifier implements Notifier {
    @Override
    public void doNotify(EventRegister events) {
        throw new NotYetImplementedException("Метод doNotify не реализован");
    }
}
