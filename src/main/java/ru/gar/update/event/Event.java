package ru.gar.update.event;

public class Event {
    private final EventType eventType;
    private final String event;

    public Event(EventType eventType, String event) {
        this.eventType = eventType;
        this.event = event;
    }

    public Event(EventType error, Throwable e) {
        this(error, e. getMessage());
    }

    public EventType getEventType() {
        return eventType;
    }

    public String getEvent() {
        return event;
    }

    public boolean isError() {
        return EventType.ERROR.equals(eventType);
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventType=" + eventType +
                ", event='" + event + '\'' +
                '}';
    }
}
