package ru.gar.update.event;

import java.util.List;

public interface Notifier {
    void doNotify(EventRegister events);
}
