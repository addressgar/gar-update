package ru.gar.update.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/*@Component
@Qualifier("garLoadingNotifier")
@Scope("singleton")*/
public class StubNotifier implements Notifier {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void doNotify(EventRegister events) {
        if(events == null) {
            return;
        }

        if(events.getEvents().size() > 0) {
            String eventsString = events.getEvents().stream()
                                    .map(Object::toString)
                                    .collect(Collectors.joining(", \n"));
            if (events.hasErrorEvents()) {
                logger.error(String.format("Оповещение о событиях:\n%s", eventsString));
            } else {
                logger.info(String.format("Оповещение о событиях:\n%s", eventsString));
            }
        }
    }
}
