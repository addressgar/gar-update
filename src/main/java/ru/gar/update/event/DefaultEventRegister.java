package ru.gar.update.event;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Qualifier("garLoadingEventRegister")
@Scope("singleton")
public class DefaultEventRegister implements EventRegister {
    private final List<Event> events = new ArrayList<>();
    private final Notifier notifier;

    public DefaultEventRegister(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public void addEvent(Event event) {
        events.add(event);
    }

    @Override
    public List<Event> getEvents() {
        return new ArrayList<>(events);
    }

    @Override
    public void clearEvents() {
        events.clear();
    }

    @Override
    public boolean hasErrorEvents() {
        return events.stream()
                .anyMatch(Event::isError);
    }

    @Override
    public void sendNotifications() {
        notifier.doNotify(this);
    }
}
