package ru.gar.update.event;

import java.util.List;

public class EventMessage {
    private final EventType eventType;
    private final List<Event> events;

    public EventMessage(EventRegister register) {
        super();
        this.events = register.getEvents();
        this.eventType = register.hasErrorEvents()? EventType.ERROR: EventType.SUCCESS;
    }

    public EventType getEventType() {
        return eventType;
    }

    public List<Event> getEvents() {
        return events;
    }
}
