package ru.gar.update.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import ru.gar.update.broker.BrokerProducer;
import ru.gar.update.broker.LoadingStateMessage;
import ru.gar.update.broker.Message;

@Component
@Qualifier("garLoadingNotifier")
@Scope("singleton")
public class BrokerNotifier implements Notifier  {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String QUEUE = "gar.loading.event";

    private final BrokerProducer brokerProducer;

    @Override
    public void doNotify(EventRegister events) {
        EventMessage eventMessage = new EventMessage(events);
        Message message = new LoadingStateMessage(eventMessage);

        if(brokerProducer == null) {
            logger.warn(String.format("Не установлен брокер сообщений. Попытка отправить в очередь \"%s\" сообщение: \"%s\"", QUEUE, message.getMessage()));
        }
        else {
            brokerProducer.sendToQueue(QUEUE, message);
        }
    }

    @Autowired
    public BrokerNotifier(@Qualifier("activeMqBrokerProducer") @Nullable BrokerProducer brokerProducer) {
        this.brokerProducer = brokerProducer;
    }
}
