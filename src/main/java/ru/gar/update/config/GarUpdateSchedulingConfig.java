package ru.gar.update.config;

import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import ru.gar.update.batch.LoadDirectoryIntoDbBatchRunner;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
@EnableScheduling
public class GarUpdateSchedulingConfig  implements SchedulingConfigurer {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private int counter = 0;

    private final LoadDirectoryIntoDbBatchRunner loadDirectoryIntoDbBatchRunner;

    @Value("${scheduler.fixed.delay:}")
    private String fixedDelay;
    @Value("${scheduler.cron:}")
    private String cron;

    public GarUpdateSchedulingConfig(@Autowired LoadDirectoryIntoDbBatchRunner loadDirectoryIntoDbBatchRunner) {
        this.loadDirectoryIntoDbBatchRunner = loadDirectoryIntoDbBatchRunner;
    }

    @Bean
    public Executor taskExecutor() {
      return Executors.newScheduledThreadPool(100);
    }

    @Bean
    @ConditionalOnExpression("${scheduler.runonce:false}")
    @Scheduled(fixedRate = 0L)
    public void runOnce() {
        loadDirectoryIntoDbBatchRunner.run();
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutor());
        if (!StringUtils.isEmpty(fixedDelay)) {
            taskRegistrar.addFixedDelayTask(loadDirectoryIntoDbBatchRunner::run, Long.parseLong(fixedDelay));
        }
        if (!StringUtils.isEmpty(cron)) {
            taskRegistrar.addCronTask(loadDirectoryIntoDbBatchRunner::run, cron);
        }
    }
}
