package ru.gar.update.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ExecutionConfig {
    @Value("${max.thread.count:0}")
    private Integer maxThreadCount;

    @Bean
    @Qualifier("garThreadExecutor")
    public ThreadPoolTaskExecutor garThreadExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(getMaxPoolSize());
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

    private int getMaxPoolSize() {
        int poolSize = Runtime.getRuntime().availableProcessors();
        if(maxThreadCount != null && maxThreadCount > 0 && maxThreadCount < poolSize) {
            poolSize = maxThreadCount;
        }

        return poolSize;
    }
}
