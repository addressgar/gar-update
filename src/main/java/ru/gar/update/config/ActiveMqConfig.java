package ru.gar.update.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.lang.Nullable;

@Configuration
@EnableJms
public class ActiveMqConfig {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Value("${spring.activemq.user}")
    private String user;

    @Value("${spring.activemq.password}")
    private String password;

    @Bean
    @Nullable
    public ActiveMQConnectionFactory connectionFactory(){
        return isConnectionParamsCorrect()? getConnectionFactory(): null;
    }

    private ActiveMQConnectionFactory getConnectionFactory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(brokerUrl);
        connectionFactory.setPassword(user);
        connectionFactory.setUserName(password);
        return connectionFactory;
    }

    private boolean isConnectionParamsCorrect() {
        boolean correct = true;

        if (StringUtils.isEmpty(brokerUrl)) {
            logger.warn("Не задан брокер сообщений");
            correct = false;
        }
        else if (StringUtils.isEmpty(user) || StringUtils.isEmpty(password)) {
            logger.warn("Некорректно заданы параметры подключения к брокеру сообщений");
            correct = false;
        }

        return correct;
    }


    @Bean
    @Qualifier("activeMqJmsTemplate")
    public JmsTemplate jmsTemplate(){
        if (connectionFactory() == null) {
            return null;
        }
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        return template;
    }
}
