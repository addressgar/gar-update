package ru.gar.update.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.gar.update.db.LoadableRegister;
import ru.gar.update.db.loader.Loadable;
import ru.gar.update.db.loader.LoaderFor;

@Component
public class LoadableRegisterFiller implements BeanPostProcessor {
    private final LoadableRegister garLoader;

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof Loadable
                && bean.getClass().isAnnotationPresent(LoaderFor.class)) {
            LoaderFor loaderFor = bean.getClass().getAnnotation(LoaderFor.class);
            garLoader.registerLoaderBean(loaderFor.value(), (Loadable)bean);
        }

        return null;
    }

    @Autowired
    @Lazy
    public LoadableRegisterFiller(LoadableRegister garLoader) {
        this.garLoader = garLoader;
    }
}
