package ru.gar.update.config;

import ru.gar.update.xjb.common.*;

import java.util.Arrays;

public enum GarDirectoryType {
    ADDRESS_OBJECTS(ADDRESSOBJECTS.class, "AS_ADDR_OBJ_"),
    ADDRESS_OBJECT_TYPES(ADDRESSOBJECTTYPES.class, "AS_ADDR_OBJ_TYPES_"),
    ADMINISTRATIVE_HIERARCHY(ru.gar.update.xjb.admhierarchy.ITEMS.class, "AS_ADM_HIERARCHY_"),
    APARTMENT_TYPES(APARTMENTTYPES.class, "AS_APARTMENT_TYPES_"),
    APARTMENTS(APARTMENTS.class, "AS_APARTMENTS_"),
    CARPLACES(CARPLACES.class,  "AS_CARPLACES_"),
    HOUSE_TYPES(HOUSETYPES.class, "AS_HOUSE_TYPES_"),
    ADD_HOUSE_TYPES(HOUSETYPES.class,  "AS_ADDHOUSE_TYPES_"),
    HOUSES(HOUSES.class, "AS_HOUSES_"),
    MUNICIPAL_HIERARCHY(ru.gar.update.xjb.munhierarchy.ITEMS.class, "AS_MUN_HIERARCHY_"),
    OBJECT_LEVELS(OBJECTLEVELS.class, "AS_OBJECT_LEVELS_"),
    ROOM_TYPES(ROOMTYPES.class, "AS_ROOM_TYPES_"),
    ROOMS(ROOMS.class, "AS_ROOMS_"),
    STEADS(STEADS.class, "AS_STEADS_");

    private final Class<?> xjbClass;
    private final String fileStartWith;

    GarDirectoryType(Class<?> xjbClass, String fileStartWith) {
        this.xjbClass = xjbClass;
        this.fileStartWith = fileStartWith.toLowerCase();
    }

    public Class getXjbClass() {
        return xjbClass;
    }

    public String getFileStartWith() {
        return fileStartWith;
    }

    public static GarDirectoryType getGarDirectoryType(String filename) {
        String filenameLowerCase = filename.toLowerCase();
        return Arrays.stream(GarDirectoryType.values())
                .filter(f -> filenameLowerCase.startsWith(f.fileStartWith)
                            && nextCharIsDigit(filenameLowerCase, f.fileStartWith)
                            && filenameLowerCase.endsWith(".xml"))
                .findFirst()
                .orElse(null);
    }

    private static boolean nextCharIsDigit(String filename, String fileStartWith) {
        if (filename == null
                || fileStartWith == null
                || filename.length() <= fileStartWith.length()) {
            return false;
        }
        char ch = filename.charAt(fileStartWith.length());
        return Character.isDigit(ch);
    }
}
